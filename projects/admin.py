from django.contrib import admin
from projects.models import Project


class ProjectsAdmin(admin.ModelAdmin):
    list_display = ["name", "owner", "id"]


# Register your models here.


admin.site.register(Project, ProjectsAdmin)
